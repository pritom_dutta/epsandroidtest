/**
 * Created by Pritom Dutta on 13/1/23.
 */

object Versions {

    const val coreKtx = "1.9.0"
    const val kotlin = "1.7.20"

    const val appCompat = "1.5.1"
    const val lifecycleRuntimeKTX = "2.3.1"

    const val material = "1.7.0"

    const val constraintLayout = "2.1.4"

    const val glide = "4.12.0"

    const val hilt_version = "2.44"

    const val coroutines_version = "1.6.4"
    const val retrofit = "2.9.0"
    const val gson = "2.8.9"
    const val loggingInterceptor = "3.1.0"
    const val moshi = "1.12.0"

    const val lifecycle_version = "2.5.1"

    const val sdpssp = "1.0.6"

    const val paging_version = "3.1.0"

    //Test
    const val jUnit = "4.13.2"
    const val ext_jUnit = "1.1.4"
    const val espresso_core = "3.5.0"
    const val truth_version = "1.1.3"

}

/**
 * define dependencies
 */
object KotlinDependencies {
    val kotlinStd = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
}

object AndroidXSupportDependencies {
    val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    val lifecycleRuntimeKTX =
        "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycleRuntimeKTX}"
}

object TestingDependencies {
    val jUnit_lib = "junit:junit:${Versions.jUnit}"
    val ext_jUnit_lib = "androidx.test.ext:junit:${Versions.ext_jUnit}"
    val espresso_core_lib = "androidx.test.espresso:espresso-core:${Versions.espresso_core}"
    val truth_test_lib = "com.google.truth:truth:${Versions.truth_version}"
    val coroutines_test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines_version}"
}

object MaterialDesignDependencies {
    val materialDesign = "com.google.android.material:material:${Versions.material}"
}

object LibraryDependencies {
    val coroutines_lib =  "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines_version}"
    val coroutines_android_lib =  "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines_version}"

    val hilt_android_lib =  "com.google.dagger:hilt-android:${Versions.hilt_version}"
    val hilt_android_compiler_lib =  "com.google.dagger:hilt-android-compiler:${Versions.hilt_version}"
    val hiltAnnotationProcessor = "com.google.dagger:hilt-android-compiler:${Versions.hilt_version}"

    val sdp = "com.intuit.sdp:sdp-android:${Versions.sdpssp}"
    val ssp = "com.intuit.ssp:ssp-android:${Versions.sdpssp}"

    val moshi = "com.squareup.moshi:moshi:${Versions.moshi}"
    val moshiKotlinCodeGen = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"
    val moshiKotlin = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"

    val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    val glideKapt = "com.github.bumptech.glide:compiler:${Versions.glide}"

    //retrofit
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    val retrofitMoshiConverter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    val retrofitRxAdapter = "com.squareup.retrofit2:adapter-rxjava3:${Versions.retrofit}"
    val gsonCon = "com.google.code.gson:gson:${Versions.gson}"

    val lifecycle_viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle_version}"

    val paging = "androidx.paging:paging-runtime-ktx:${Versions.paging_version}"
    val rxpaging = "androidx.paging:paging-rxjava3:${Versions.paging_version}"

    //logging interceptor
    val loggingInterceptor = "com.github.ihsanbal:LoggingInterceptor:${Versions.loggingInterceptor}"
}