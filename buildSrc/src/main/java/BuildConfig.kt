/**
 * Created by Pritom Dutta on 13/1/23.
 */
object BuildConfig {
    const val applicationID = "com.pritom.dutta.epsandroidtest"
    const val compileSdkVersion = 33
    const val buildToolsVersion = "33.0.1"
    const val minSdkVersion = 22
    const val targetSdkVersion = 32
    const val versionCode = 1
    const val versionName = "1.0.1"

    const val testRunner = "androidx.test.runner.AndroidJUnitRunner"
}