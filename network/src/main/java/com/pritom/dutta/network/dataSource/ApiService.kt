package com.pritom.dutta.network.dataSource

import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.model.TmdbApiResponse
import com.pritom.dutta.network.utils.DataSourceConstants
import com.pritom.dutta.network.utils.NetworkResult
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Pritom Dutta on 13/1/23.
 */

interface ApiService {
    @GET("movie/popular?api_key=${DataSourceConstants.API_KEY}&language=en-US")
    suspend fun fetchTopRatedMovie(@Query("page") page: Int): NetworkResult<TmdbApiResponse>

    @GET("movie/{id}?api_key=${DataSourceConstants.API_KEY}&language=en-US")
    suspend fun fetchTopRatedMovieDetails(@Path("id") id: Int): NetworkResult<MovieDetails>
}