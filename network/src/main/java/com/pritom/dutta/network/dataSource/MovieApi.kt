package com.pritom.dutta.network.dataSource

import android.util.Log
import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.model.TmdbApiResponse
import com.pritom.dutta.network.utils.NetworkResult
import com.pritom.dutta.network.utils.onError
import com.pritom.dutta.network.utils.onException
import com.pritom.dutta.network.utils.onSuccess
import javax.inject.Inject

/**
 * Created by Pritom Dutta on 13/1/23.
 */
class MovieApi @Inject constructor(
    private val apiService: ApiService
) {
    suspend operator fun invoke(page: Int): NetworkResult<TmdbApiResponse> {
        return apiService.fetchTopRatedMovie(page)
    }

    suspend fun getMovieDetails(id: Int): NetworkResult<MovieDetails> {
        return apiService.fetchTopRatedMovieDetails(id)
    }
}