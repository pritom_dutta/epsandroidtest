package com.pritom.dutta.network.utils

/**
 * Created by Pritom Dutta on 13/1/23.
 */
object DataSourceConstants {
    const val API_KEY = "dfc67e319cc457c3d0d20948a046b160"
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val IMAGE_URL = "https://image.tmdb.org/t/p/original/"
}