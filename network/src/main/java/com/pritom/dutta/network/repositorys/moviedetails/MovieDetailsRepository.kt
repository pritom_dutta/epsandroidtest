package com.pritom.dutta.network.repositorys.moviedetails

import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.utils.NetworkResult

/**
 * Created by Pritom Dutta on 13/1/23.
 */
interface MovieDetailsRepository {
   suspend fun getMovieDetails(id: Int): NetworkResult<MovieDetails>
}