package com.pritom.dutta.network.model

data class TmdbApiResponse(
    val page: Int?,
    val results: List<MovieDetails>,
    val total_pages: Int?,
    val total_results: Int?,
    val success: Boolean?
)

data class TMDBErrorResponse(
    val status_code: Int?,
    val status_message: String?
)