package com.pritom.dutta.network.repositorys.toprated

import androidx.paging.PagingData
import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.model.TmdbApiResponse
import com.pritom.dutta.network.utils.NetworkResult
import kotlinx.coroutines.flow.Flow

/**
 * Created by Pritom Dutta on 13/1/23.
 */
interface TopRateMovieRepository {
    fun getTopRatedMovie(): Flow<PagingData<MovieDetails>>
}