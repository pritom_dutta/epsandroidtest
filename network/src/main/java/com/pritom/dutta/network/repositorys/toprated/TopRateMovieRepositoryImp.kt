package com.pritom.dutta.network.repositorys.toprated

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.pritom.dutta.network.dataSource.MovieApi
import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.model.TmdbApiResponse
import com.pritom.dutta.network.utils.NetworkResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Created by Pritom Dutta on 13/1/23.
 */

class TopRateMovieRepositoryImp @Inject constructor(private val api: MovieApi) :
    TopRateMovieRepository {

    override fun getTopRatedMovie(): Flow<PagingData<MovieDetails>> = Pager(
        config = PagingConfig(
            pageSize = 20,
            maxSize = 10000,
            enablePlaceholders = false
        ),
        pagingSourceFactory = { MovieListPagingSource(api) }
    ).flow
}