package com.pritom.dutta.network.utils

import com.pritom.dutta.network.model.TMDBErrorResponse
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response


/**
 * Created by Pritom Dutta on 13/1/23.
 */

fun <T : Any> handleApi(
    execute: () -> Response<T>
): NetworkResult<T> {
    return try {
        val response = execute()
        val body = response.body()

        if (response.isSuccessful && body != null) {
            NetworkResult.Success(body)
        } else {
            val errorResponse = convertErrorBody(response.errorBody())
            val errorMessage: String? = errorResponse?.status_message
            NetworkResult.Error(
                code = response.code(),
                message = errorMessage ?: response.message()
            )
        }
    } catch (e: HttpException) {
        NetworkResult.Error(code = e.code(), message = e.message())
    } catch (e: Throwable) {
        NetworkResult.Exception(e)
    }
}

// If you don't wanna handle api's own
// custom error response then ignore this function
private fun convertErrorBody(errorBody: ResponseBody?): TMDBErrorResponse? {
    return try {
        errorBody?.source()?.let {
            val moshiAdapter = Moshi.Builder().build().adapter(TMDBErrorResponse::class.java)
            moshiAdapter.fromJson(it)
        }
    } catch (exception: Exception) {
        TMDBErrorResponse(404, exception.message)
    }
}