package com.pritom.dutta.network.repositorys.toprated

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.pritom.dutta.network.dataSource.MovieApi
import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.model.TmdbApiResponse
import com.pritom.dutta.network.utils.onError
import com.pritom.dutta.network.utils.onException
import com.pritom.dutta.network.utils.onSuccess
import okhttp3.internal.wait
import java.lang.Exception

/**
 * Created by Pritom Dutta on 13/1/23.
 */
class MovieListPagingSource(
    private val api: MovieApi
) : PagingSource<Int, MovieDetails>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MovieDetails> {
        return try {
            val position = params.key ?: 1
            var res = TmdbApiResponse(0, emptyList(), 0, 0, false)
            val response =  api.invoke(position)
                .onSuccess { data ->
                    res = data
                }
            return LoadResult.Page(
                data = res.results,
                prevKey = if (position == 1) null else position - 1,
                nextKey = if (position == res.total_pages) null else position + 1
            )

        } catch (e: Exception) {
            Log.e("ExceptionPa", "load: "+e.message )
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, MovieDetails>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}