package com.pritom.dutta.network.repositorys.moviedetails

import com.pritom.dutta.network.dataSource.MovieApi
import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.utils.NetworkResult
import javax.inject.Inject

/**
 * Created by Pritom Dutta on 13/1/23.
 */
class MovieDetailsRepositoryImp @Inject constructor(private val api: MovieApi) :
    MovieDetailsRepository {
    override suspend fun getMovieDetails(id: Int): NetworkResult<MovieDetails> =
        api.getMovieDetails(id)
}