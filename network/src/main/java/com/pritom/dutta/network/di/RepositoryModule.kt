package com.pritom.dutta.network.di

import com.pritom.dutta.network.repositorys.moviedetails.MovieDetailsRepository
import com.pritom.dutta.network.repositorys.moviedetails.MovieDetailsRepositoryImp
import com.pritom.dutta.network.repositorys.toprated.TopRateMovieRepository
import com.pritom.dutta.network.repositorys.toprated.TopRateMovieRepositoryImp
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Pritom Dutta on 13/1/23.
 */

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun provideTopRateMovieRepository(api: TopRateMovieRepositoryImp): TopRateMovieRepository

    @Binds
    @Singleton
    abstract fun provideMovieDetailsRepository(api: MovieDetailsRepositoryImp): MovieDetailsRepository
}