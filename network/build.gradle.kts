plugins {
    id ("com.android.library")
    id ("org.jetbrains.kotlin.android")
    id ("kotlin-kapt")
    id("com.google.dagger.hilt.android")
}

android {
    namespace = BuildConfig.applicationID
    compileSdk = BuildConfig.compileSdkVersion

    defaultConfig {
        minSdk = BuildConfig.minSdkVersion
        targetSdk = BuildConfig.targetSdkVersion

        testInstrumentationRunner = BuildConfig.testRunner
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation (KotlinDependencies.coreKtx)
    implementation (AndroidXSupportDependencies.appCompat)

    //Hilt
    implementation(LibraryDependencies.hilt_android_lib)
    kapt(LibraryDependencies.hilt_android_compiler_lib)

    implementation(LibraryDependencies.paging)
    implementation(LibraryDependencies.rxpaging)

    //Coroutines
    implementation(LibraryDependencies.coroutines_lib)
    implementation(LibraryDependencies.coroutines_android_lib)

    //Retrofit
    implementation(LibraryDependencies.retrofit)
    implementation(LibraryDependencies.retrofitMoshiConverter)
    implementation(LibraryDependencies.retrofitRxAdapter)

    implementation(LibraryDependencies.loggingInterceptor) {
        exclude(group = "org.json", module = "json")
    }

    //Test Library
    testImplementation (TestingDependencies.jUnit_lib)
    androidTestImplementation (TestingDependencies.ext_jUnit_lib)
    testImplementation(TestingDependencies.truth_test_lib)
    testImplementation(TestingDependencies.coroutines_test)
}