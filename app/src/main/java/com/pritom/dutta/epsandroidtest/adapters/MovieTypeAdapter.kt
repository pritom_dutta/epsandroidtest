package com.pritom.dutta.epsandroidtest.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pritom.dutta.epsandroidtest.adapters.viewModel.ItemMovieTypeViewModel
import com.pritom.dutta.epsandroidtest.databinding.ItemMovieTypeBinding
import com.pritom.dutta.epsandroidtest.ui.Base.BaseViewHolder
import com.pritom.dutta.network.model.GenresModel

/**
 * Created by Pritom Dutta on 13/1/23.
 */

class MovieTypeAdapter(private var list: List<GenresModel>) :
    RecyclerView.Adapter<BaseViewHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    fun setData(list: List<GenresModel>){
        this.list = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val item = ItemMovieTypeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ItemMovieTypeViewHolder(item)
    }

    inner class ItemMovieTypeViewHolder(private val mBinding: ItemMovieTypeBinding) :
        BaseViewHolder(mBinding.root) {

        private var mItemBinding: ItemMovieTypeViewModel? = null
        override fun onBind(position: Int) {
            mItemBinding = ItemMovieTypeViewModel(list.get(position).name)
            mBinding.viewModel = mItemBinding
            mBinding.executePendingBindings()
        }
    }
}