package com.pritom.dutta.epsandroidtest.adapters.viewModel

import androidx.databinding.ObservableField

/**
 * Created by Pritom Dutta on 13/1/23.
 */
class ItemMovieTypeViewModel(name: String) {
    var title: ObservableField<String>? = null
    init {
        this.title = ObservableField(name)
    }
}