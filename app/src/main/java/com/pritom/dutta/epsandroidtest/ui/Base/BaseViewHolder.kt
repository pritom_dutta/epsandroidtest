package com.pritom.dutta.epsandroidtest.ui.Base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Pritom Dutta on 13/1/23.
 */
abstract class BaseViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
    abstract fun onBind(position: Int)
}