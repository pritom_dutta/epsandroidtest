package com.pritom.dutta.epsandroidtest.ui.Details

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.pritom.dutta.epsandroidtest.R
import com.pritom.dutta.epsandroidtest.adapters.MovieTypeAdapter
import com.pritom.dutta.epsandroidtest.databinding.ActivityMovieDetailsBinding
import com.pritom.dutta.epsandroidtest.helper.EqualSpacingItemDecoration
import com.pritom.dutta.epsandroidtest.utils.loadImage
import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.utils.DataSourceConstants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class MovieDetailsActivity : AppCompatActivity() {

    companion object {
        @JvmStatic
        fun newIntent(context: Context, id: Int): Intent =
            Intent(context, MovieDetailsActivity::class.java)
                .putExtra("movieID", id)
    }

    private val viewModel: MovieDetailsViewModel by viewModels()
    private lateinit var binding: ActivityMovieDetailsBinding
    private lateinit var aMovieTypeAdapter: MovieTypeAdapter
    private var id: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }
        id = intent.getIntExtra("movieID", 0)
        binding = DataBindingUtil.setContentView(
            this@MovieDetailsActivity,
            R.layout.activity_movie_details
        )

        binding.imgBack.setOnClickListener { onBackPressed() }
        setupRecyclerView()

        viewModel.getMovieDetails(id)
        getData()
        ifApiGetError()
    }

    private fun setupRecyclerView() {
        aMovieTypeAdapter = MovieTypeAdapter(emptyList())
        val mLayoutManager = FlexboxLayoutManager(this)
        mLayoutManager.flexDirection = FlexDirection.ROW
        mLayoutManager.justifyContent = JustifyContent.FLEX_START
        binding.rvMovieType.layoutManager = mLayoutManager
        binding.rvMovieType.adapter = aMovieTypeAdapter
        if (binding.rvMovieType.itemDecorationCount == 0) {
            binding.rvMovieType.addItemDecoration(EqualSpacingItemDecoration(10))
        }
    }

    private fun getData() {
        lifecycleScope.launch {
            viewModel._postStateFlow.collect { data ->
                withContext(Dispatchers.Main) {
                    binding.progressBar.visibility = View.GONE
                    binding.container.visibility = View.VISIBLE
                    setMovieData(data)
                }
            }
        }
    }

    private fun setMovieData(data: MovieDetails) {
        binding.imgMoviePoster.loadImage("${DataSourceConstants.IMAGE_URL}${data.poster_path}")
        binding.tvRating.text = String.format("%.1f", data.vote_average)
        binding.tvMovieName.text = "${data.title}"
        binding.tvMovieDetails.text = "${data.overview}"
//        binding.tvRelease.text = "${data.status}"
        aMovieTypeAdapter.setData(data.genres ?: emptyList())
    }

    private fun ifApiGetError() {
        lifecycleScope.launch {
            viewModel._errorFlow.collect {
                withContext(Dispatchers.Main) {
                    binding.progressBar.visibility = View.GONE
                }
            }
        }
    }
}