package com.pritom.dutta.epsandroidtest.ui.Home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.model.TmdbApiResponse
import com.pritom.dutta.network.repositorys.toprated.TopRateMovieRepository
import com.pritom.dutta.network.utils.onError
import com.pritom.dutta.network.utils.onException
import com.pritom.dutta.network.utils.onSuccess
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Pritom Dutta on 13/1/23.
 */

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: TopRateMovieRepository) :
    ViewModel() {

//    private val postStateFlow by lazy {
//        MutableStateFlow(TmdbApiResponse(0, emptyList(), 0, 0, false))
//    }
//    val _postStateFlow: StateFlow<TmdbApiResponse> = postStateFlow
//
//    private val errorFlow: MutableStateFlow<String> = MutableStateFlow("")
//    val _errorFlow: StateFlow<String> = errorFlow

    fun getTopRatedMovie() : Flow<PagingData<MovieDetails>> {
        return repository.getTopRatedMovie().cachedIn(viewModelScope)
    }
}