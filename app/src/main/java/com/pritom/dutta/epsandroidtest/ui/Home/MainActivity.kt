package com.pritom.dutta.epsandroidtest.ui.Home

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pritom.dutta.epsandroidtest.R
import com.pritom.dutta.epsandroidtest.adapters.MoviePagingAdapter
import com.pritom.dutta.epsandroidtest.databinding.ActivityMainBinding
import com.pritom.dutta.epsandroidtest.helper.GridSpacingItemDecoration
import com.pritom.dutta.epsandroidtest.ui.Details.MovieDetailsActivity
import com.pritom.dutta.network.model.MovieDetails
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    val TAG = "MainActivity"
    lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()
    private lateinit var aMoviePagingAdapter: MoviePagingAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)

        // RecyclerView SetUp
        aMoviePagingAdapter = MoviePagingAdapter()
        var mGridLayoutManager = GridLayoutManager(this, 3)
        mGridLayoutManager.orientation = RecyclerView.VERTICAL
        binding.rvMovie.layoutManager = mGridLayoutManager
        binding.rvMovie.setHasFixedSize(true)
        binding.rvMovie.adapter = aMoviePagingAdapter

        aMoviePagingAdapter.setListener { movieDetails ->
            startActivity(MovieDetailsActivity.newIntent(this, movieDetails.id ?: 0))
        }

        if (binding.rvMovie.itemDecorationCount == 0) {
            val spanCount = 3
            val spacing = 40
            val includeEdge = true
            binding.rvMovie.addItemDecoration(
                GridSpacingItemDecoration(
                    spanCount,
                    spacing,
                    includeEdge
                )
            )
        }

        lifecycleScope.launch {
            aMoviePagingAdapter.loadStateFlow.collectLatest { loadStates ->
                val loading = loadStates.refresh is LoadState.Loading
                if (loading) {
                    binding.progressBar.visibility = View.VISIBLE
                } else {
                    binding.progressBar.visibility = View.GONE
                }
            }
        }
        getData()
    }

    private fun getData() {
        lifecycleScope.launch {
            viewModel.getTopRatedMovie().collect {
                aMoviePagingAdapter.submitData(it)
            }
        }
    }

}