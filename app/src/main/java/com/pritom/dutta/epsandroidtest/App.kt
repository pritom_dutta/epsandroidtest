package com.pritom.dutta.epsandroidtest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Pritom Dutta on 13/1/23.
 */

@HiltAndroidApp
class App: Application() {
}