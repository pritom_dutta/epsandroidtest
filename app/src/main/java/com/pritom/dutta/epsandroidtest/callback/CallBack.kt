package com.pritom.dutta.epsandroidtest.callback

import androidx.appcompat.widget.AppCompatImageView
import com.pritom.dutta.network.model.MovieDetails

/**
 * Created by Pritom Dutta on 13/1/23.
 */


typealias OnOptionClickItem = (model: MovieDetails) -> Unit