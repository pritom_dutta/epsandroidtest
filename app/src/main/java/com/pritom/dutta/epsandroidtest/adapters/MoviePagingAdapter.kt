package com.pritom.dutta.epsandroidtest.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.pritom.dutta.epsandroidtest.adapters.viewModel.ItemMovieViewModel
import com.pritom.dutta.epsandroidtest.callback.OnOptionClickItem
import com.pritom.dutta.epsandroidtest.databinding.ItemMovieBinding
import com.pritom.dutta.epsandroidtest.ui.Base.BaseViewHolder
import com.pritom.dutta.network.model.MovieDetails

/**
 * Created by Pritom Dutta on 13/1/23.
 */

class MoviePagingAdapter : PagingDataAdapter<MovieDetails, BaseViewHolder>(COMPARATOR) {

    var callback: OnOptionClickItem? = null
    //
    fun setListener(callbackClick: OnOptionClickItem) {
        this.callback = callbackClick
    }

    inner class ItemViewHolder(private val mBinding: ItemMovieBinding) :
        BaseViewHolder(mBinding.root) {

        private var mItemBinding: ItemMovieViewModel? = null
        override fun onBind(position: Int) {
            mItemBinding = ItemMovieViewModel(
                getItem(position)?.title ?: "",
                getItem(position)?.poster_path ?: "",
                callback!!,
                getItem(position)!!
            )
            mBinding.viewModel = mItemBinding
            mBinding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val item = ItemMovieBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ItemViewHolder(item)
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<MovieDetails>() {
            override fun areItemsTheSame(oldItem: MovieDetails, newItem: MovieDetails): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MovieDetails, newItem: MovieDetails): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }
}