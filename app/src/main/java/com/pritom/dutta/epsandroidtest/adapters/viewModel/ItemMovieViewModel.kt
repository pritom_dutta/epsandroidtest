package com.pritom.dutta.epsandroidtest.adapters.viewModel

import androidx.databinding.ObservableField
import com.pritom.dutta.epsandroidtest.callback.OnOptionClickItem
import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.utils.DataSourceConstants

/**
 * Created by Pritom Dutta on 13/1/23.
 */
class ItemMovieViewModel(title: String,img: String,private val callbackClick: OnOptionClickItem, private val model: MovieDetails) {
    var name: ObservableField<String>? = null
    var imgurl: ObservableField<String>? = null
    init {
        this.name = ObservableField(title)
        this.imgurl = ObservableField("${DataSourceConstants.IMAGE_URL}${img}")
    }

    fun onClick(){
        if (callbackClick != null){
            callbackClick.invoke(model)
        }
    }
}