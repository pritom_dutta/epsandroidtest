package com.pritom.dutta.epsandroidtest.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.pritom.dutta.epsandroidtest.R
import com.pritom.dutta.epsandroidtest.ui.Base.GlideApp

/**
 * Created by Pritom Dutta on 13/1/23.
 */

@BindingAdapter("image_url")
fun ImageView.loadImage(url: String?) {
    if (!url.isNullOrEmpty()) {
        GlideApp.with(this)
            .load(url)
            .error(R.drawable.ic_broken_image)
            .skipMemoryCache( false)
            .into(this)
    } else {
        GlideApp.with(this)
            .load(R.drawable.movie_dummy)
            .into(this)
    }
}