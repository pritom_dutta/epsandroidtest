package com.pritom.dutta.epsandroidtest.ui.Details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pritom.dutta.network.model.MovieDetails
import com.pritom.dutta.network.repositorys.moviedetails.MovieDetailsRepository
import com.pritom.dutta.network.utils.onError
import com.pritom.dutta.network.utils.onException
import com.pritom.dutta.network.utils.onSuccess
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Pritom Dutta on 13/1/23.
 */

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(private val repository: MovieDetailsRepository) :
    ViewModel(){

    private val postStateFlow by lazy {
        MutableStateFlow(MovieDetails(false,"", emptyList(), emptyList(),
            0,"","","",0.0,"",
            "","","",false,0.0,0))
    }
    val _postStateFlow: StateFlow<MovieDetails> = postStateFlow

    private val errorFlow: MutableStateFlow<String> = MutableStateFlow("")
    val _errorFlow: StateFlow<String> = errorFlow

    fun getMovieDetails(id: Int) {
        viewModelScope.launch {
            val response = repository.getMovieDetails(id)
            response.onSuccess { res ->
                postStateFlow.emit(res)
            }.onError { code, message ->
                errorFlow.emit("Error Code: $code,\nMessage: $message")
            }.onException { error ->
                errorFlow.emit("${error.message}")
            }
        }
    }
}