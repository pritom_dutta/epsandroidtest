plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-android")
    id("kotlin-kapt")
    id("com.google.dagger.hilt.android")
}

android {
    namespace = BuildConfig.applicationID
    compileSdk = BuildConfig.compileSdkVersion

    defaultConfig {
        applicationId = BuildConfig.applicationID
        minSdk = BuildConfig.minSdkVersion
        targetSdk = BuildConfig.targetSdkVersion
        versionCode = BuildConfig.versionCode
        versionName = BuildConfig.versionName

        testInstrumentationRunner = BuildConfig.testRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        dataBinding = true
    }
}

dependencies {

    implementation(KotlinDependencies.coreKtx)
    implementation(KotlinDependencies.kotlinStd)
    implementation("androidx.annotation:annotation-experimental:1.3.0")
    implementation("androidx.navigation:navigation-fragment-ktx:2.5.3")
    implementation(AndroidXSupportDependencies.lifecycleRuntimeKTX)

    implementation(AndroidXSupportDependencies.appCompat)
    implementation(AndroidXSupportDependencies.constraintLayout)

    implementation(MaterialDesignDependencies.materialDesign)
    implementation("com.google.android.flexbox:flexbox:3.0.0")

    //sdp-ssp
    implementation(LibraryDependencies.ssp)
    implementation(LibraryDependencies.sdp)

    implementation(LibraryDependencies.paging)
    implementation(LibraryDependencies.rxpaging)

    //glide
    implementation(LibraryDependencies.glide)
    kapt(LibraryDependencies.glideKapt)

    // ViewModel
    implementation(LibraryDependencies.lifecycle_viewmodel)

    //Hilt
    implementation(LibraryDependencies.hilt_android_lib)
    implementation("androidx.appcompat:appcompat:1.6.0")
    implementation("com.google.android.material:material:1.7.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    kapt(LibraryDependencies.hilt_android_compiler_lib)
    kapt(LibraryDependencies.hiltAnnotationProcessor)

    //Coroutines
    implementation(LibraryDependencies.coroutines_lib)
    implementation(LibraryDependencies.coroutines_android_lib)

    implementation(project(":network"))

    //Test Lib
    testImplementation(TestingDependencies.jUnit_lib)
    androidTestImplementation(TestingDependencies.ext_jUnit_lib)
    androidTestImplementation(TestingDependencies.espresso_core_lib)
}